<?php
/**
 * Fired during plugin activation
 *
 * @link       https://club.wpeka.com
 * @since      1.0.0
 *
 * @package    Woo_Auction_Daily_Refresh_Products
 * @subpackage Woo_Auction_Daily_Refresh_Products/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Woo_Auction_Daily_Refresh_Products
 * @subpackage Woo_Auction_Daily_Refresh_Products/includes
 * @author     wpeka <example@wpeka.com>
 */
class Woo_Auction_Daily_Refresh_Products_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		if ( ! wp_next_scheduled( 'woo_auction_daily_refresh_products_daily_cron' ) ) {
			wp_schedule_event( strtotime( 'tomorrow' ), 'daily', 'woo_auction_daily_refresh_products_daily_cron' );
		}
		if ( ! wp_next_scheduled( 'woo_auction_daily_refresh_products_penny_bid_cron' ) ) {
			wp_schedule_event( time(), 'every_fifteen_minutes', 'woo_auction_daily_refresh_products_penny_bid_cron' );
		}
	}

}
