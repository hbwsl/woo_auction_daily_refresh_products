<?php
/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://club.wpeka.com
 * @since      1.0.0
 *
 * @package    Woo_Auction_Daily_Refresh_Products
 * @subpackage Woo_Auction_Daily_Refresh_Products/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Woo_Auction_Daily_Refresh_Products
 * @subpackage Woo_Auction_Daily_Refresh_Products/includes
 * @author     wpeka <example@wpeka.com>
 */
class Woo_Auction_Daily_Refresh_Products_I18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'woo_auction_daily_refresh_products',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
