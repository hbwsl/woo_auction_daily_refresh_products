<?php
/**
 * Fired during plugin deactivation
 *
 * @link       https://club.wpeka.com
 * @since      1.0.0
 *
 * @package    Woo_Auction_Daily_Refresh_Products
 * @subpackage Woo_Auction_Daily_Refresh_Products/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Woo_Auction_Daily_Refresh_Products
 * @subpackage Woo_Auction_Daily_Refresh_Products/includes
 * @author     wpeka <example@wpeka.com>
 */
class Woo_Auction_Daily_Refresh_Products_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		wp_clear_scheduled_hook( 'woo_auction_daily_refresh_products_daily_cron' );
		wp_clear_scheduled_hook( 'woo_auction_daily_refresh_products_penny_bid_cron' );
	}

}
