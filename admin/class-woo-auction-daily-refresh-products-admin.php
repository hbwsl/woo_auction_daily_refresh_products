<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://club.wpeka.com
 * @since      1.0.0
 *
 * @package    Woo_Auction_Daily_Refresh_Products
 * @subpackage Woo_Auction_Daily_Refresh_Products/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Woo_Auction_Daily_Refresh_Products
 * @subpackage Woo_Auction_Daily_Refresh_Products/admin
 * @author     wpeka <example@wpeka.com>
 */
class Woo_Auction_Daily_Refresh_Products_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $plugin_name       The name of this plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Add custom cron schedules.
	 *
	 * @param Array $schedules Cron schedules.
	 * @return mixed
	 */
	public function woo_auction_daily_refresh_products_cron_schedules( $schedules ) {
		$schedules['every_minute']        = array(
			'interval' => 60,
			'display'  => __( 'Every Minute', 'woo_auction_daily_refresh_products' ),
		);
		$schedules['every_three_minutes'] = array(
			'interval' => 180,
			'display'  => __( 'Every 3 Minutes', 'woo_auction_daily_refresh_products' ),
		);
		$schedules['every_five_minutes']  = array(
			'interval' => 300,
			'display'  => __( 'Every 5 Minutes', 'woo_auction_daily_refresh_products' ),
		);

		$schedules['every_fifteen_minutes'] = array(
			'interval' => 900,
			'display'  => __( 'Every 15 Minutes', 'woo_auction_daily_refresh_products' ),
		);
		return $schedules;
	}

	/**
	 * Place random user bids.
	 */
	public function woo_auction_daily_refresh_products_daily_cron_penny_tasks() {
		$args = array(
			'post_type'      => 'product',
			'posts_per_page' => -1,
		);

		$loop     = new WP_Query( $args );
		$users    = get_users( array( 'role' => 'shop_manager' ) );
		$user_ids = array();
		foreach ( $users as $user ) {
			$user_ids[] = $user->ID;
		}
		if ( ! empty( $user_ids ) ) {
			while ( $loop->have_posts() ) :
				$loop->the_post();
				global $product;
				if ( 'auction_simple' === $product->get_type() || 'auction_penny' === $product->get_type() || 'auction_reverse' === $product->get_type() ) {
					$auction_start_price      = get_post_meta( $product->get_id(), 'auction_current_bid' );
					$auction_bid_increment    = get_post_meta( $product->get_id(), 'auction_bid_increment' );
					$auction_highest_bid_user = get_post_meta( $product->get_id(), 'auction_highest_bid_user' );
					$user_id                  = $user_ids[ array_rand( $user_ids ) ];
					$post_id                  = $product->get_id();
					$next_bid                 = $auction_start_price[0] + $auction_bid_increment[0];
					if ( $user_id !== $auction_highest_bid_user[0] ) {
						update_post_meta( $post_id, 'auction_current_bid', $next_bid );
						update_post_meta( $post_id, 'auction_highest_bid', $next_bid );
						update_post_meta( $post_id, 'auction_highest_bid_user', $user_id );
						$result = WC_Auction_Software_Helper::set_auction_bid_logs( $user_id, $post_id, $next_bid, current_time( 'mysql' ) );
					}
				}
			endwhile;

			wp_reset_query();
		}
	}

	/**
	 * Daily cron tasks for creating fresh auction products.
	 *
	 * @throws Object WC_Data_Exception Data exception.
	 */
	public function woo_auction_daily_refresh_products_daily_cron_tasks() {
		$args = array(
			'post_type'      => 'product',
			'posts_per_page' => -1,
		);

		$loop = new WP_Query( $args );

		while ( $loop->have_posts() ) :
			$loop->the_post();
			global $product;
			if ( 'auction_simple' === $product->get_type() || 'auction_penny' === $product->get_type() || 'auction_reverse' === $product->get_type() ) {
				$sku                              = $product->get_sku();
				$image_id                         = $product->get_image_id();
				$gallery_image_ids                = $product->get_gallery_image_ids();
				$description                      = $product->get_description();
				$cat_ids                          = $product->get_category_ids();
				$auction_item_condition           = get_post_meta( $product->get_id(), 'auction_item_condition', true );
				$auction_start_price              = get_post_meta( $product->get_id(), 'auction_start_price', true );
				$auction_bid_increment            = get_post_meta( $product->get_id(), 'auction_bid_increment', true );
				$auction_reserve_price            = get_post_meta( $product->get_id(), 'auction_reserve_price', true );
				$auction_reserve_price_reverse    = get_post_meta( $product->get_id(), 'auction_reserve_price_reverse', true );
				$auction_reserve_price_penny      = get_post_meta( $product->get_id(), 'auction_reserve_price_penny', true );
				$auction_buy_it_now_price         = get_post_meta( $product->get_id(), 'auction_buy_it_now_price', true );
				$auction_buy_it_now_price_penny   = get_post_meta( $product->get_id(), 'auction_buy_it_now_price_penny', true );
				$auction_buy_it_now_price_reverse = get_post_meta( $product->get_id(), 'auction_buy_it_now_price_reverse', true );
				$auction_extend_or_relist_auction = get_post_meta( $product->get_id(), 'auction_extend_or_relist_auction', true );
				if ( ! get_option( 'auction_extend_relist_settings_updated' ) ) {
					$auction_date_from = get_post_meta( $product->get_id(), 'auction_date_from', true );
					$date_time_from    = datetime::createfromformat( 'Y-m-d H:i:s', $auction_date_from );
					$auction_date_to   = get_post_meta( $product->get_id(), 'auction_date_to', true );
					$date_time_to      = datetime::createfromformat( 'Y-m-d H:i:s', $auction_date_to );
					$diff              = intval( ( $date_time_to->getTimestamp() - $date_time_from->getTimestamp() ) / 60 );

					$if_fail     = get_post_meta( $product->get_id(), 'auction_' . $auction_extend_or_relist_auction . '_if_fail', true );
					$if_not_paid = get_post_meta( $product->get_id(), 'auction_' . $auction_extend_or_relist_auction . '_if_not_paid', true );
					$duration    = get_post_meta( $product->get_id(), 'auction_' . $auction_extend_or_relist_auction . '_duration', true );
					$always      = get_post_meta( $product->get_id(), 'auction_' . $auction_extend_or_relist_auction . '_always', true );

					if ( 'yes' !== $if_fail && '' !== $if_fail ) {
						$wait_time_before_if_fail = $if_fail;
						$duration_if_fail         = $diff;
						$if_fail                  = 'yes';
					}
					if ( 'yes' !== $if_not_paid && '' !== $if_not_paid ) {
						$wait_time_before_if_not_paid = $if_not_paid;
						$duration_if_not_paid         = $diff;
						$if_not_paid                  = 'yes';
					}

					if ( 'yes' !== $always && '' !== $duration ) {
						$always                  = 'yes';
						$wait_time_before_always = $duration;
						$duration_always         = $diff;
					}
					add_option( 'auction_extend_relist_settings_updated', true );

				} else {
					$if_fail                      = get_post_meta( $product->get_id(), 'auction_' . $auction_extend_or_relist_auction . '_if_fail', true );
					$wait_time_before_if_fail     = get_post_meta( $product->get_id(), 'auction_wait_time_before_' . $auction_extend_or_relist_auction . '_if_fail', true );
					$duration_if_fail             = get_post_meta( $product->get_id(), 'auction_' . $auction_extend_or_relist_auction . '_duration_if_fail', true );
					$if_not_paid                  = get_post_meta( $product->get_id(), 'auction_' . $auction_extend_or_relist_auction . '_if_not_paid', true );
					$wait_time_before_if_not_paid = get_post_meta( $product->get_id(), 'auction_wait_time_before_' . $auction_extend_or_relist_auction . '_if_not_paid', true );
					$duration_if_not_paid         = get_post_meta( $product->get_id(), 'auction_' . $auction_extend_or_relist_auction . '_duration_if_not_paid', true );
					$always                       = get_post_meta( $product->get_id(), 'auction_' . $auction_extend_or_relist_auction . '_always', true );
					$wait_time_before_always      = get_post_meta( $product->get_id(), 'auction_wait_time_before_' . $auction_extend_or_relist_auction . '_always', true );
					$duration_always              = get_post_meta( $product->get_id(), 'auction_' . $auction_extend_or_relist_auction . '_duration_always', true );

				}

				wp_delete_post( $product->get_id(), true );

				$obj_product = new WC_Product();
				$obj_product->set_name( $product->get_name() );
				$obj_product->set_status( 'publish' );
				$obj_product->set_sku( $sku );
				$obj_product->product_type = $product->get_type();
				$obj_product->add_meta_data( 'auction_item_condition', $auction_item_condition );
				$obj_product->add_meta_data( 'auction_start_price', $auction_start_price );
				$obj_product->add_meta_data( 'auction_bid_increment', $auction_bid_increment );
				$obj_product->add_meta_data( 'auction_date_from', current_time( 'mysql' ) );

				$date_time_from = datetime::createfromformat( 'Y-m-d H:i:s', current_time( 'mysql' ) );
				$date_time_to   = $date_time_from;
				$date_time_to   = $date_time_to->add( new DateInterval( 'P1M' ) );

				$obj_product->add_meta_data( 'auction_date_to', $date_time_to->format( 'Y-m-d H:i:s' ) );

				$obj_product->add_meta_data( 'auction_reserve_price', $auction_reserve_price );
				$obj_product->add_meta_data( 'auction_reserve_price_reverse', $auction_reserve_price_reverse );
				$obj_product->add_meta_data( 'auction_reserve_price_penny', $auction_reserve_price_penny );
				$obj_product->add_meta_data( 'auction_buy_it_now_price', $auction_buy_it_now_price );
				$obj_product->add_meta_data( 'auction_buy_it_now_price_penny', $auction_buy_it_now_price_penny );
				$obj_product->add_meta_data( 'auction_buy_it_now_price_reverse', $auction_buy_it_now_price_reverse );
				$obj_product->add_meta_data( 'auction_extend_or_relist_auction', $auction_extend_or_relist_auction );
				$obj_product->add_meta_data( 'auction_' . $auction_extend_or_relist_auction . '_if_fail', $if_fail );
				$obj_product->add_meta_data( 'auction_wait_time_before_' . $auction_extend_or_relist_auction . '_if_fail', $wait_time_before_if_fail );
				$obj_product->add_meta_data( 'auction_' . $auction_extend_or_relist_auction . '_duration_if_fail', $duration_if_fail );
				$obj_product->add_meta_data( 'auction_' . $auction_extend_or_relist_auction . '_if_not_paid', $if_not_paid );
				$obj_product->add_meta_data( 'auction_wait_time_before_' . $auction_extend_or_relist_auction . '_if_not_paid', $wait_time_before_if_not_paid );
				$obj_product->add_meta_data( 'auction_' . $auction_extend_or_relist_auction . '_duration_if_not_paid', $duration_if_not_paid );
				$obj_product->add_meta_data( 'auction_' . $auction_extend_or_relist_auction . '_always', $always );
				$obj_product->add_meta_data( 'auction_wait_time_before_' . $auction_extend_or_relist_auction . '_always', $wait_time_before_always );
				$obj_product->add_meta_data( 'auction_' . $auction_extend_or_relist_auction . '_duration_always', $duration_always );
				$obj_product->add_meta_data( 'auction_errors', '' );

				$obj_product->set_description( $description );
				$obj_product->set_image_id( $image_id );
				$obj_product->set_gallery_image_ids( $gallery_image_ids );
				$obj_product->set_category_ids( $cat_ids );
				$obj_product->save();
			}
		endwhile;
		wp_reset_query();

		$loop     = new WP_Query( $args );
		$users    = get_users( array( 'role' => 'shop_manager' ) );
		$user_ids = array();
		foreach ( $users as $user ) {
			$user_ids[] = $user->ID;
		}
		if ( ! empty( $user_ids ) ) {
			while ( $loop->have_posts() ) :
				$loop->the_post();
				global $product;
				if ( 'auction_simple' === $product->get_type() || 'auction_penny' === $product->get_type() || 'auction_reverse' === $product->get_type() ) {
					$woo_auction_start_price   = get_post_meta( $product->get_id(), 'auction_start_price' );
					$woo_auction_bid_increment = get_post_meta( $product->get_id(), 'auction_bid_increment' );
					$user_id                   = $user_ids[ array_rand( $user_ids ) ];
					$post_id                   = $product->get_id();
					$next_bid                  = $woo_auction_start_price[0] + $woo_auction_bid_increment[0];
					update_post_meta( $post_id, 'auction_initial_bid_placed', 1 );
					update_post_meta( $post_id, 'auction_current_bid', $next_bid );
					update_post_meta( $post_id, 'auction_highest_bid', $next_bid );
					update_post_meta( $post_id, 'auction_highest_bid_user', $user_id );
					$result = WC_Auction_Software_Helper::set_auction_bid_logs( $user_id, $post_id, $next_bid, current_time( 'mysql' ) );
				}
			endwhile;

			wp_reset_query();
		}

	}

	/**
	 * Get product by SKU.
	 *
	 * @param String $sku Product SKU.
	 * @return null|WC_Product
	 */
	public function get_product_by_sku( $sku ) {
		global $wpdb;

		$product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku ) );

		if ( $product_id ) {
			return new WC_Product( $product_id );
		}

		return null;
	}

}
