<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://club.wpeka.com
 * @since             1.0.0
 * @package           Woo_Auction_Daily_Refresh_Products
 *
 * @wordpress-plugin
 * Plugin Name:       Woo Auction Daily Refresh Products
 * Plugin URI:        https://club.wpeka.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            wpeka
 * Author URI:        https://club.wpeka.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woo_auction_daily_refresh_products
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WOO_AUCTION_DAILY_REFRESH_PRODUCTS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woo_auction_daily_refresh_products-activator.php
 */
function activate_woo_auction_daily_refresh_products() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woo-auction-daily-refresh-products-activator.php';
	Woo_Auction_Daily_Refresh_Products_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woo_auction_daily_refresh_products-deactivator.php
 */
function deactivate_woo_auction_daily_refresh_products() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woo-auction-daily-refresh-products-deactivator.php';
	Woo_Auction_Daily_Refresh_Products_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_woo_auction_daily_refresh_products' );
register_deactivation_hook( __FILE__, 'deactivate_woo_auction_daily_refresh_products' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woo-auction-daily-refresh-products.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_woo_auction_daily_refresh_products() {

	$plugin = new Woo_Auction_Daily_Refresh_Products();
	$plugin->run();

}
run_woo_auction_daily_refresh_products();
